import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

import { Symptom, Risk, ResultData } from '../models/models';
import { url, options, bodyTemplate } from '../api/api-service'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private http: HttpClient, public alertController: AlertController) {}

  ageField: any;
  sexField: any;
  risksField: any = [];
  symptomsField: any = [];
  submit = "Soumettre";
  details = false;
  
  resultData: ResultData;
  body = bodyTemplate;

  callApi() {
    if (this.ageField > 0 && this.ageField <= 120) {
      if (this.sexField == 'female' || this.sexField == 'male') {
        if (this.risksField.length != 0 && this.symptomsField.length != 0) {
          this.copyValuesToBody()
          this.submit = "Analyse..."
          // Api call
          this.http.post(url, this.body, options).subscribe(data => {
            this.submit = "Soumettre"
            this.resultData = new ResultData(
              Math.round(data["data"][0].prediction[0].probabilityDecimal*100),
              Math.round(data["data"][0].prediction[1].probabilityDecimal*100),
              Math.round(data["data"][0].prediction[2].probabilityDecimal*100),
              Math.round(data["data"][0].prediction[3].probabilityDecimal*100),
              Math.round(data["data"][0].prediction[4].probabilityDecimal*100),
              Math.round(data["data"][0].prediction[5].probabilityDecimal*100),
              data["data"][0].prediction[6].outcome.coding[0].code,
              Math.round(data["data"][0].prediction[6].probabilityDecimal*100)
            )
          }, error => {
            console.log(error);
            this.submit = "Soumettre"
            this.presentAlert("Erreur", "Une erreur est survenue, veuillez réessayer plus tard.");
          })
        } else {
          this.presentAlert("Données invalides", "Veuillez sélectionner au minimum un risque et un symptôme.");
        }
      } else {
        this.presentAlert("Données invalides", "Veuillez sélectionner un sexe.");
      }
    } else {
      this.presentAlert("Données invalides", "L'âge doit être compris entre 1 et 120.");
    }
  }

  copyValuesToBody() {
    this.body[0].component[0].valueQuantity.value = this.ageField;
    this.body[0].component[1].valueQuantity.value = this.sexField == "female" ? 0 : 1;
    for (var risk of this.risksField) {
      var riskData: Risk = new Risk(risk, risk);
      this.body[0].component[2].code.coding.push(riskData);
    }
    for (var symptom of this.symptomsField) {
      var symptomData: Symptom = new Symptom(symptom, symptom);
      this.body[0].component[3].code.coding.push(symptomData);
    }
  }

  async presentAlert(header: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['Ok']
    });

    await alert.present();
  }

  analyseResults(): string {
    let message: string;
    if (this.resultData.summary == "hospitalise") {
      message = "Nos modèles prédisent à " + this.resultData.probabilityDecimal + "% que le patient devrait être hospitalisé."
    } else {
      message = "Nos modèles prédisent à " + this.resultData.probabilityDecimal + "% que le patient ne devrait pas être hospitalisé."
    }
    return message;
  }

  showDetails() {
    this.details = !this.details;
  }

}
