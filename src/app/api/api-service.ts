import { HttpHeaders } from '@angular/common/http';

export let url = "https://canalytics.comunicare.io/api/predictionHospitalizationCovidFhir";
export let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
export let options = { headers: headers };

export var bodyTemplate = [{
    "subject": {
        "reference": "123456789", // Math.random().toString
        "display": "Jean-Claude Van Damme"
      },
    "issued": "2021", //Date.now().toString()
    "component": [
      {
        "valueQuantity": { 
          "value": null
        }, 
        "code": {
          "coding": [ {
            "code": "age",
            "display": "age",
            "system": "http://comunicare.io"
          }] 
        }
      },
      {
        "valueQuantity": { 
          "value": null
        }, 
        "code": {
          "coding": [ {
            "code": "sexe",
            "display": "sexe",
            "system": "http://comunicare.io"
          }] 
        }
      },
      {
        "valueQuantity": { 
          "value": 1
        }, 
        "code": {
          "coding": [] 
        }
      },
      {
        "valueQuantity": { 
          "value": 1
        }, 
        "code": {
          "coding": [] 
        }
      }
    ] 
  }]