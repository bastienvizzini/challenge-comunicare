export class Risk {
    code: string;
    display: string;
    system: string;

    constructor(code: string, display: string) {
        this.code = code;
        this.display = display;
        this.system = "http://comunicare.io";
    }
}

export class Symptom {
    code: string;
    display: string;
    system: string;

    constructor(code: string, display: string) {
        this.code = code;
        this.display = display;
        this.system = "http://comunicare.io";
    }
}

export class ResultData {
    RFAmbulatoire: number;
    RFHospitalise: number;
    NNAmbulatoire: number;
    NNHospitalise: number;
    GBTAmbulatoire: number;
    GBTHospitalise: number;
    summary: string;
    probabilityDecimal: number;

    constructor(
        RFAmbulatoire: number, 
        RFHospitalise: number, 
        NNAmbulatoire: number, 
        NNHospitalise: number,
        GBTAmbulatoire: number,
        GBTHospitalise: number,
        summary: string,
        probabilityDecimal: number) {
            this.RFAmbulatoire = RFAmbulatoire;
            this.RFHospitalise = RFHospitalise;
            this.NNAmbulatoire = NNAmbulatoire;
            this.NNHospitalise = NNHospitalise;
            this.GBTAmbulatoire = GBTAmbulatoire;
            this.GBTHospitalise = GBTHospitalise;
            this.summary = summary;
            this.probabilityDecimal = probabilityDecimal;
        }
}